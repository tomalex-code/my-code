<?php

namespace App\Services;

use GuzzleHttp\Client;
use Exception;
use Cache;

class SendPulseService
{
    public static function addEmailToAddressBook(string $addressBookId, array $data): bool
    {
        try {
            $emails = [
                [
                    'email' => $data['email'],
                    'variables' => $data,
                ]
            ];
            $addData = array(
                'emails' => json_encode($emails),
            );

            $client = new Client([
                'http_errors' => false
            ]);

            $response = $client->request(
                'POST',
                'https://api.sendpulse.com/addressbooks/' . $addressBookId . '/emails',
                [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer ' . self::getToken(),
                    ],
                    'form_params' => $addData,
                ]
            );

            $responseStatusCode = $response->getStatusCode();

            if ($responseStatusCode == 200) {
                return true;
            } else {
                if ($responseStatusCode === 401) {
                    Cache::forget('sendpulse_access_token');

                    return self::addEmailToAddressBook($addressBookId, $data);
                } else {
                    return false;
                }
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public static function deleteEmailFromAddressBook(string $addressBookId, string $email): bool
    {
        try {
            $delData = [
                'emails' => json_encode([$email]),
            ];

            $client = new Client([
                'http_errors' => false
            ]);

            $response = $client->request(
                'DELETE',
                'https://api.sendpulse.com/addressbooks/' . $addressBookId . '/emails',
                [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer ' . self::getToken(),
                    ],
                    'form_params' => $delData,
                ]
            );

            $responseStatusCode = $response->getStatusCode();

            if ($responseStatusCode == 200) {
                return true;
            } else {
                if ($responseStatusCode === 401) {
                    Cache::forget('sendpulse_access_token');

                    return self::deleteEmailFromAddressBook($addressBookId, $email);
                } else {
                    return false;
                }
            }
        } catch (Exception $e) {
            return false;
        }
    }

    private static function getToken()
    {
        if (Cache::has('sendpulse_access_token')) {
            $token = json_decode(Cache::get('sendpulse_access_token'));

            return $token->access_token;
        } else {
            try {
                $sendpulseClientId = config('services.sendpulse.client_id');
                $sendpulseClientSecret = config('services.sendpulse.client_secret');

                $client = new Client([
                    'http_errors' => false
                ]);

                $responseToken = $client->request(
                    'POST',
                    'https://api.sendpulse.com/oauth/access_token',
                    [
                        'form_params' => [
                            'grant_type' => 'client_credentials',
                            'client_id' => $sendpulseClientId,
                            'client_secret' => $sendpulseClientSecret,
                        ]
                    ]
                );

                if ($responseToken->getStatusCode() === 200) {
                    $token = json_decode($responseToken->getBody()->getContents());

                    Cache::put('sendpulse_access_token', json_encode($token), $token->expires_in);

                    return $token->access_token;
                }
            } catch (Exception $e) {
                return null;
            }
        }
    }
}
