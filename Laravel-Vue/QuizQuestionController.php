<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Quiz\QuestionRequest;
use App\Quiz;
use App\QuizQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuizQuestionController extends Controller
{
    public function store(QuestionRequest $request, Quiz $quiz)
    {
        $data = array_merge(['order' => QuizQuestion::max('order') + 1], $request->validated());

        $question = $quiz->questions()->create($data);

        $question->answers()->createMany($data['answers']);
    }

    public function update(QuestionRequest $request, Quiz $quiz, QuizQuestion $question)
    {
        $data = $request->validated();

        $question->update($data);

        $question->answers()->delete();

        $question->answers()->createMany($data['answers']);
    }

    public function delete(Request $request, Quiz $quiz, QuizQuestion $question)
    {
        $question->delete();
    }

    public function order(Request $request)
    {
        $ids = $request->ids;
        foreach ($ids as $key => $id) {
            QuizQuestion::whereId($id)->update(['order' => $key]);
        }
    }
}
